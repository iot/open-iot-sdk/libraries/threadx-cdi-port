# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.21)

# Test on the Corstone-300 FVP only, because the CMSIS-RTOSv2 adaptation layer
# which we validate does not depend on any specific architecture, but the
# CMSIS-RTOSv2 validation suite depends on CMSIS Core which requires one/any
# Cortex core
set(CMAKE_SYSTEM_PROCESSOR cortex-m55)

set(FETCHCONTENT_QUIET OFF)

include(FetchContent)

# Toolchain files need to exist before first call to project
FetchContent_Declare(
    iotsdk-toolchains
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/toolchain.git
    GIT_TAG         6133b1de96ca069bcdd2e74876e62a338bf5c928
    SOURCE_DIR      ${CMAKE_BINARY_DIR}/toolchains
)
FetchContent_MakeAvailable(iotsdk-toolchains)

project(threadx-cdi-port-test LANGUAGES C CXX ASM)

# ThreadX variables must be set before adding ThreadX
set(THREADX_ARCH cortex_m55)
if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(THREADX_TOOLCHAIN gnu)
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(THREADX_TOOLCHAIN ac6)
else()
    message(FATAL_ERROR "Unsupported compiler: ${CMAKE_C_COMPILER_ID}")
endif()

set(THREADX_CDI_TEST_CONFIG "default" CACHE STRING "Test config to use when testing the threadx-cdi-port")
if(NOT EXISTS ${CMAKE_CURRENT_LIST_DIR}/configs/${THREADX_CDI_TEST_CONFIG})
    message(FATAL_ERROR "Cannot find test config '${THREADX_CDI_TEST_CONFIG}'")
endif()
set(THREADX_CDI_TEST_CONFIG_DIR ${CMAKE_CURRENT_LIST_DIR}/configs/${THREADX_CDI_TEST_CONFIG})

set(TX_USER_FILE ${THREADX_CDI_TEST_CONFIG_DIR}/threadx-config/tx_user.h)

# Add CMSIS-RTOSv2 adaptation layer and ThreadX
add_subdirectory(.. threadx-cdi-port-build)

# Add Open IoT SDK for CMSIS Core and MCU Driver HAL platforms
# Note: To avoid circular dependency, we do *not* enable ThreadX and the
# CMSIS-RTOSv2 adaptation layer through the SDK
set(IOTSDK_CMSIS_RTOS_API ON)
set(IOTSDK_MDH_ARM ON)
FetchContent_Declare(
    open-iot-sdk
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git
    GIT_TAG         5a822d2becc0c07da68e78e0895a818589131cb6
)
FetchContent_MakeAvailable(open-iot-sdk)

target_compile_definitions(cmsis-core INTERFACE CMSIS_device_header="ARMCM55.h")
target_include_directories(cmsis-core INTERFACE cmsis-config)

FetchContent_Declare(
  cmsis-rtos2-validation
  GIT_REPOSITORY https://github.com/ARM-software/CMSIS-RTOS2_Validation.git
  GIT_TAG        7f79fd8bf754098a28a4f13c21de635d5908ec0a
)
FetchContent_MakeAvailable(cmsis-rtos2-validation)

add_library(cmsis-rtos2-validation-tests
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/Config/RV2_Config.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/Config/RV2_Config_Device.h
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Thread.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Common.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_EventFlags.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_GenWait.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Kernel.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_MemoryPool.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_MessageQueue.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Mutex.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Semaphore.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_ThreadFlags.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/RV2_Timer.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/cmsis_rv2.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/tf_main.c
    ${cmsis-rtos2-validation_SOURCE_DIR}/Source/tf_report.c
)

target_include_directories(cmsis-rtos2-validation-tests
    PUBLIC
        ${cmsis-rtos2-validation_SOURCE_DIR}/Include
)

target_link_libraries(cmsis-rtos2-validation-tests
    PUBLIC
        cmsis-core
        threadx-cdi-port
)

target_include_directories(cmsis-rtos2-validation-tests PUBLIC ${THREADX_CDI_TEST_CONFIG_DIR}/RV2-config)

# Enable checks from project_options.
FetchContent_Declare(project_options
    GIT_REPOSITORY  https://github.com/cpp-best-practices/project_options.git
    GIT_TAG         v0.24.1
)
FetchContent_MakeAvailable(project_options)
include(${project_options_SOURCE_DIR}/Index.cmake)
include(${project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

set(GCC_WARNINGS -Wall -Wextra -Wpedantic)
set(CLANG_WARNINGS ${GCC_WARNINGS})

dynamic_project_options(
    CLANG_WARNINGS
    ${CLANG_WARNINGS}
    GCC_WARNINGS
    ${GCC_WARNINGS}
)

# Apply to threadx-cdi-port compiler flags from project_options and project_warnings
target_link_libraries(threadx-cdi-port
    PRIVATE
        project_options
        project_warnings
)

# Set executable suffix to be toolchain-independent for ease of testing
set(CMAKE_EXECUTABLE_SUFFIX .elf)

add_executable(threadx-cdi-port-tests main.c)

target_link_libraries(threadx-cdi-port-tests
    cmsis-rtos2-validation-tests

    mcu-driver-hal
    iotsdk-serial-retarget
    cmsis-startup-cortex-m
    mdh-arm-an552-mps3

    project_options
    project_warnings
)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/gcc.ld)
    target_link_options(threadx-cdi-port-tests PRIVATE -T ${linker_script})
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/armclang.sct)
    target_link_options(threadx-cdi-port-tests PRIVATE --scatter=${linker_script})
endif()
set_target_properties(threadx-cdi-port-tests PROPERTIES LINK_DEPENDS ${linker_script})

include(CTest)

add_test(
    NAME    threadx-cdi-port-tests
    COMMAND htrun
        --image-path=threadx-cdi-port-tests.elf
        --compare-log=${THREADX_CDI_TEST_CONFIG_DIR}/test.log
        --sync=0 --baud-rate=115200 --polling-timeout=120
        --micro=FVP_CS300_U55 --fm=MPS3
    COMMAND_EXPAND_LISTS
)
