/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "hal/serial_api.h"
#include "cmsis_rv2.h"
#include "mps3_uart.h"

#include <assert.h>
#include <stdio.h>

static mdh_serial_t *serial_obj = NULL;

static int serial_setup()
{
    mps3_uart_t *uart;
    if (mps3_uart_init(&uart, &UART0_CMSDK_DEV_NS)) {
        serial_obj = &(uart->serial);
    } else {
        return -1;
    }

    mdh_serial_set_baud(serial_obj, 115200);

    return 0;
}

// Use by the CMSIS-RTOSv2 validation suite for printing
int stdout_putchar(int ch)
{
    assert(serial_obj != NULL);

    mdh_serial_put_data(serial_obj, (uint32_t)ch);

    return ch;
}

int main(void)
{
    if (serial_setup() != 0) {
        printf("Failed to set up MCU Driver HAL serial\r\n");
        return -1;
    }

    // Run tests (this always returns 0)
    (void)cmsis_rv2();

    return 0;
}
