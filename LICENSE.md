Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license, as can be found in: LICENSE-apache-2.0.txt

Folders containing files under different permissive license than Apache 2.0 are listed below. Each folder should contain its own README file with license specified for its files. The original license text is included in those source files.

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields":[
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items" :[
        {
            "Component": "ThreadX",
            "Path": ".",
            "License": "Microsoft Software License Terms for Microsoft Azure RTOS",
            "Origin": "https://github.com/azure-rtos/threadx",
            "Category": "2",
            "Version": "v6.1.12_rel",
            "Security risk": "low"
        },
        {
            "Component": "CMSIS-RTOS2_Validation",
            "Path": "tests",
            "License": "Apache 2.0",
            "Origin": "https://github.com/ARM-software/CMSIS-RTOS2_Validation",
            "Category": "2",
            "Version": "7f79fd8bf754098a28a4f13c21de635d5908ec0a",
            "Security risk": "low"
        }
    ]
}
```
