# CMSIS-RTOSv2 Adaptation Layer for Azure RTOS ThreadX

This project implements the [CMSIS-RTOSv2 API][1] on top of [Azure RTOS
ThreadX][2]. It enables a single application to use software components that are
based on two different RTOS APIs.

## Files

The CMSIS-RTOS2 adaptation layer consists of the following files:
-   `include/cmsis_os2.h`
-   `src/tx_cmsis_os2.c`

## ThreadX Configuration

### CMake Configuration

The variable `TX_USER_FILE` must be set to the *full path* to your own
`tx_user.h` (whose use is detailed below) *before* fetching `threadx-cdi-port`,
for example:

```cmake
set(TX_USER_FILE ${CMAKE_CURRENT_LIST_DIR}/threadx-config/tx_user.h)
```

### ThreadX User Configuration Header

The following lines must be added to the `tx_user.h` configuration file:

```c
#define TX_PORT_SPECIFIC_PRE_SCHEDULER_INITIALIZATION return;
#define TX_THREAD_USER_EXTENSION VOID *tx_cmsis_extension;
```

To configure how many bytes of memory are used for the CMSIS layer's
internal heap, define `OS_DYNAMIC_MEM_SIZE` as below:

```c
#define OS_DYNAMIC_MEM_SIZE 0x4000
```

To configure the default stack size of a thread created with
`osThreadNew()`, define `OS_STACK_SIZE` as below:

```c
#define OS_STACK_SIZE 1024
```

To enable round-robin thread time-slicing and configure the time-slice
duration, define `OS_ROBIN_ENABLE` and `OS_ROBIN_TIMEOUT` as below:

```c
/* Enable time-slicing */
#define OS_ROBIN_ENABLE     1
/* Set time-slice to 7 ticks */
#define OS_ROBIN_TIMEOUT    7
```

Note that ThreadX must also be configured to support at least 64
priority levels. To do this, add the line:

```c
#define TX_MAX_PRIORITIES 64
```

to the `tx_user.h` configuration file.

In order to enable the `osKernelGetTickFreq()` function, the value
`TX_TIMER_TICKS_PER_SECOND` must be defined. This is the value that will be
returned by `osKernelGetTickFreq()`. Ensure that it matches the tick frequency
configured in the port-specific `tx_initialize_low_level()` function otherwise
`osKernelGetTickFreq()` will return an incorrect result.

To use the `osThreadGetStackSpace()` function, ensure that the macro
`TX_DISABLE_STACK_FILLING` is _not_ defined.

#### Required macro for Armv8-M

If your target architecture is based on Armv8-M such as Cortex-M55, you *must*
define either `TX_SINGLE_MODE_SECURE` (TrustZone not enabled) or
`TX_SINGLE_MODE_NON_SECURE` (TrustZone enabled with ThreadX used in Non-Secure
Processing Environment), because this adaptation layer supports single-mode
only which is the most commonly used. For example:

```c
#define TX_SINGLE_MODE_SECURE
```

#### Thread Memory Saving

The amount of memory used for each thread's control block can be reduced by
disabling some features if they are not used. These features are as follows:
* Joinable threads, disabled by setting `TX_CMSIS_JOINABLE` to `0`.
* Thread flags, disabled by setting `TX_CMSIS_THREAD_FLAGS` to `0`.

## Limitations

### Message Queues
The message queue implementation is based on ThreadX's queue. As a result, the
CMSIS-RTOSv2 message queues inherit the constraints of ThreadX's
implementation. Specifically:
* Queues are FIFO-only, CMSIS-RTOSv2 priority-sorting is ignored.
* A queue's message size can be no more than 64 bytes.
* The start of a queue's backing memory must be aligned on a 4-byte boundary.

## Validation of the Adaptation Layer

The adaptation layer has been validated using the [CMSIS-RTOSv2 Validation Test
Suite][3], with a test application in the [tests](./tests) directory that runs
the suite on Corstone-300.

To build the test application, with GCC for example:

    cmake -B __build -S tests -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
    cmake --build __build

The executable is located at `__build/threadx-cdi-port-tests.elf` which you can
pass to the Corstone-300 FVP.

## License and contributions

CMSIS-RTOSv2 Adaptation Layer for Azure RTOS ThreadX is provided under the
Apache-2.0 license. All contributions to software and documents are licensed by
contributors under the same license model as the software/document itself (ie.
inbound == outbound licensing). Open IoT SDK may reuse software already licensed
under another license, provided the license is permissive in nature and
compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are
listed in the LICENSE file.

[Azure RTOS ThreadX][2], which is not part of this repository but fetched as a
build dependency, is available under the Microsoft Software License Terms for
Microsoft Azure RTOS.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab
issue tracker. Instead, send an email to the security team at
arm-security@arm.com stating that you may have found a security vulnerability
in the Open IoT SDK.

More details can be found at [Arm Developer website][4].

[1]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/index.html
[2]: https://github.com/azure-rtos/threadx
[3]: https://github.com/ARM-software/CMSIS-RTOS2_Validation
[4]: https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities
